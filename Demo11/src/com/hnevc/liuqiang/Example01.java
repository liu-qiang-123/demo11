package com.hnevc.liuqiang;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 使用字节流对于文件的读取操作
 * IO流分为字节流、字符流
 * 字节流 （顶级父类不能被实例化）
 * InpuStream 输入流
 * OutputStream 输出流
 * <p>
 * FileInputStream 输入流 【读】
 * FileOutputStream 输出流 【写】
 */

public class Example01 {

    public static void main(String[] args) {
        try {
            FileOutputStream out = new FileOutputStream("text.txt");
            String str = "姓名+ 学号";
            byte[] b = str.getBytes();
 //           for (int i = 0; i < b.length; i++) {
    //            out.write(b[i]);
     //       }
            out.write(b);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
