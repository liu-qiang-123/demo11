package com.hnevc.liuqiang;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * 使用FileInputStream
 */
public class Example02 {

    public static void main(String[] args) {
        try {
            FileInputStream in = new FileInputStream("text.txt");
            int b = 0;
            while (true) {
                b = in.read();
                if (b == -1) {
                    break;
                }
                System.out.println(b);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}